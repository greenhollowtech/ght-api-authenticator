<?php

namespace GHT\ApiAuthenticator\Tests;

use GHT\ApiAuthenticator\GHTApiAuthenticator;
use phpmock\phpunit\PHPMock;
use Symfony\Component\HttpFoundation\Request;

/**
 * Exercises the GHTApiAuthenticator.
 */
class GHTApiAuthenticatorTest extends \PHPUnit_Framework_TestCase
{
    use PHPMock;

    /**
     * @var GHT\ApiAuthenticator\GHTApiAuthenticator
     */
    protected $apiAuthenticator;

    /**
     * @var array
     */
    protected $mockFunctions;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // Configure all the expected PHP functions
        $this->mockFunctions = array();
        foreach (array(
            'http_get_request_headers',
        ) as $functionName) {
            $this->mockFunctions[$functionName] = $this->getFunctionMock('GHT\ApiAuthenticator', $functionName);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->mockFunctions);
    }

    /**
     * Verify that the Authorization header can be found.
     */
    public function testGetAuthorizationHeader()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->once())
            ->will($this->returnValue(array('Authorization' => 'testAuthorizationHeader')))
        ;

        $this->assertEquals('testAuthorizationHeader', GHTApiAuthenticator::getAuthorizationHeader());
    }

    /**
     * Verify that attempting to get the Authorization header when it isn't set
     * fails quietly.
     */
    public function testGetAuthorizationHeaderWhenNotSet()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->once())
            ->will($this->returnValue(array('SomeOtherHeader' => 'testHeaderValue')))
        ;

        $this->assertEquals('', GHTApiAuthenticator::getAuthorizationHeader());
    }

    /**
     * Verify that the authorization header is parsed as expected.
     */
    public function testParseAuthorizationHeader()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->once())
            ->will($this->returnValue(array('Authorization' => 'user=testUser,key=testKey,hash=testHash')))
        ;

        $this->assertEquals(array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
            ),
            GHTApiAuthenticator::parseAuthorizationHeader()
        );
    }

    /**
     * Verify that the authorization header is parsed as expected when passing
     * the authorization header value.
     */
    public function testParseAuthorizationHeaderWithRequest()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->never());

        $header = 'user=testUser,key=testKey,hash=testHash';

        $this->assertEquals(array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
            ),
            GHTApiAuthenticator::parseAuthorizationHeader($header)
        );
    }

    /**
     * Verify that an exception is thrown when the authorization header is
     * parsed and is missing required authorization data.
     */
    public function testParseAuthorizationHeaderMissingStuff()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->once())
            ->will($this->returnValue(array('Authorization' => 'user=testUser,key=testKey,hash=')))
        ;

        $this->setExpectedException(
          'UnexpectedValueException', 'Authorization header missing hash value.'
        );

        // Trigger the exception
        GHTApiAuthenticator::parseAuthorizationHeader();
    }

    /**
     * Verify that null is returned without an exception thrown when no
     * authorization header at all is found.
     */
    public function testParseAuthorizationHeaderMissingHeader()
    {
        $this->mockFunctions['http_get_request_headers']->expects($this->once())
            ->will($this->returnValue(array('SomeOtherHeader' => 'testHeaderValue')))
        ;

        $this->assertNull(GHTApiAuthenticator::parseAuthorizationHeader());
    }

    /**
     * Verify that the authorization data is parsed from the $_POST values in
     * the request as expected.
     */
    public function testParseRequestViaPost()
    {
        $post = array(
            'user' => 'testUser',
            'key' => 'testKey',
            'hash' => 'testHash',
        );

        $request = new Request(
            array(), // $_GET
            $post, // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array() // $_SERVER
        );

        $this->assertEquals(array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
            ),
            GHTApiAuthenticator::parseRequest($request)
        );
    }

    /**
     * Verify that the authorization data is parsed from the $_POST values in
     * the request as expected.
     */
    public function testParseRequestViaGet()
    {
        $get = array(
            'user' => 'testUser',
            'key' => 'testKey',
            'hash' => 'testHash',
        );

        $request = new Request(
            $get, // $_GET
            array(), // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array() // $_SERVER
        );

        $this->assertEquals(array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
            ),
            GHTApiAuthenticator::parseRequest($request)
        );
    }

    /**
     * Verify that an exception is thrown when the authorization header is
     * parsed and is missing required authorization data.
     */
    public function testParseRequestMissingStuff()
    {
        $this->setExpectedException(
          'UnexpectedValueException', 'Request missing API user value.'
        );

        // Trigger the exception
        GHTApiAuthenticator::parseRequest(new Request());
    }

    /**
     * Verify that credentials are returned in the expected array.
     */
    public function testGetCredentials()
    {
        $request = new Request(
            array(), // $_GET
            array( // $_POST
                'data' => 'Some test data',
            ),
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'HTTP_AUTHORIZATION' => 'user=testUser,key=testKey,hash=testHash',
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80
            )
        );
        $request->setMethod('POST');

        $this->assertEquals(
            array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
                'api-data' => '{"api-key":"testKey","api-method":"POST","api-url":"http:\/\/test.greenhollowtech.com\/","data":"Some test data"}',
            ),
            GHTApiAuthenticator::getCredentials($request)
        );
    }

    /**
     * Verify that an exception is thrown when credentials are not provided via
     * the authorization header.
     */
    public function testGetCredentialsNotInHeader()
    {
        $request = new Request(
            array(), // $_GET
            array( // $_POST
                'data' => 'Some test data',
                'user' => 'testUser',
                'key' => 'testKey',
                'hash' => 'testHash',
            ),
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80
            )
        );
        $request->setMethod('POST');

        $this->setExpectedException(
          'Exception', 'Authorization header is required.'
        );

        // Trigger the exception
        GHTApiAuthenticator::getCredentials($request);
    }

    /**
     * Verify that credentials are returned in the expected array from the
     * from the request when strict mode is overridden.
     */
    public function testGetCredentialsFromRequest()
    {
        $request = new Request(
            array(), // $_GET
            array( // $_POST
                'data' => 'Some test data',
                'user' => 'testUser',
                'key' => 'testKey',
                'hash' => 'testHash',
            ),
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80
            )
        );
        $request->setMethod('POST');

        $this->assertEquals(
            array(
                'api-user' => 'testUser',
                'api-key' => 'testKey',
                'api-hash' => 'testHash',
                'api-data' => '{"api-key":"testKey","api-method":"POST","api-url":"http:\/\/test.greenhollowtech.com\/","data":"Some test data","hash":"testHash","key":"testKey","user":"testUser"}',
            ),
            GHTApiAuthenticator::getCredentials($request, false)
        );
    }

    /**
     * Verify that a request can be validated as expected.
     */
    public function testValidate()
    {
        $postData = array(
            'data' => 'Some test data',
        );

        // Compile the hash data as it would be by the GHT\PhpApi\ApiClient
        $hashData = array_merge($postData, array(
            'api-key' => 'testKey',
            'api-method' => 'POST',
            'api-url' => 'http://test.greenhollowtech.com/api/test',
        ));

        ksort($hashData);

        $authorizationHeader = sprintf(
            'user=%s,key=%s,hash=%s',
            'testUser',
            'testKey',
            hash_hmac('sha256', json_encode($hashData), 'testSecret')
        );

        // Configure the Request
        $request = new Request(
            array(), // $_GET
            $postData, // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'HTTP_AUTHORIZATION' => $authorizationHeader,
                'REQUEST_URI' => '/api/test',
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80,
            )
        );
        $request->setMethod('POST');

        // Get the request credentials
        $credentials = GHTApiAuthenticator::getCredentials($request);

        // Validate the credentials - no exception should be thrown
        try {
            GHTApiAuthenticator::validate('testKey', 'testSecret', $credentials);
            $this->assertTrue(true);
        }
        catch (\Exception $e) {
            $this->assertNull($e->getMessage());
        }
    }

    /**
     * Verify that an exception is thrown when a request with a mismatched key
     * is attempted to be validated.
     */
    public function testValidateApiKeyMismatch()
    {
        $postData = array(
            'data' => 'Some test data',
        );

        // Compile the hash data as it would be by the GHT\PhpApi\ApiClient
        $hashData = array_merge($postData, array(
            'api-key' => 'badKey',
            'api-method' => 'POST',
            'api-url' => 'http://test.greenhollowtech.com/api/test',
        ));

        ksort($hashData);

        $authorizationHeader = sprintf(
            'user=%s,key=%s,hash=%s',
            'testUser',
            'badKey',
            hash_hmac('sha256', json_encode($hashData), 'testSecret')
        );

        // Configure the Request
        $request = new Request(
            array(), // $_GET
            $postData, // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'HTTP_AUTHORIZATION' => $authorizationHeader,
                'REQUEST_URI' => '/api/test',
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80,
            )
        );
        $request->setMethod('POST');

        // Get the request credentials
        $credentials = GHTApiAuthenticator::getCredentials($request);

        $this->setExpectedException(
          'Exception', 'API key "badKey" is invalid.'
        );

        // Trigger the exception
        GHTApiAuthenticator::validate('testKey', 'testSecret', $credentials);
    }

    /**
     * Verify that an exception is thrown when a request with a bad hash from
     * mismatched secrets is attempted to be validated.
     */
    public function testValidateApiSecretMismatch()
    {
        $postData = array(
            'data' => 'Some test data',
        );

        // Compile the hash data as it would be by the GHT\PhpApi\ApiClient
        $hashData = array_merge($postData, array(
            'api-key' => 'testKey',
            'api-method' => 'POST',
            'api-url' => 'http://test.greenhollowtech.com/api/test',
        ));

        ksort($hashData);

        $authorizationHeader = sprintf(
            'user=%s,key=%s,hash=%s',
            'testUser',
            'testKey',
            hash_hmac('sha256', json_encode($hashData), 'testSecret')
        );

        // Configure the Request
        $request = new Request(
            array(), // $_GET
            $postData, // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'HTTP_AUTHORIZATION' => $authorizationHeader,
                'REQUEST_URI' => '/api/test',
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80,
            )
        );
        $request->setMethod('POST');

        // Get the request credentials
        $credentials = GHTApiAuthenticator::getCredentials($request);

        $this->setExpectedException(
          'Exception', 'API hash is invalid for data: {"api-key":"testKey","api-method":"POST","api-url":"http:\/\/test.greenhollowtech.com\/api\/test","data":"Some test data"}'
        );

        // Trigger the exception
        GHTApiAuthenticator::validate('testKey', 'badSecret', $credentials);
    }
}
