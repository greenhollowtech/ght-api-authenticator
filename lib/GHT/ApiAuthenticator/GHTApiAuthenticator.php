<?php

namespace GHT\ApiAuthenticator;

use Symfony\Component\HttpFoundation\Request;

/**
 * Authenticator for validating API request credentials.
 */
class GHTApiAuthenticator
{
    /**
     * Get the authorization header.
     *
     * @return string
     */
    public static function getAuthorizationHeader()
    {
        $headers = http_get_request_headers();

        return isset($headers['Authorization']) ? $headers['Authorization'] : '';
    }

    /**
     * Get the full set of credential information.  By default, credentials are
     * only pulled from the Authorization header.  Override the strict mode to
     * also allow for credentials in the request.
     *
     * @param Symfony\Component\HttpFoundation\Request $request A Symfony request instance.
     * @param boolean $strict Set to false to override strict mode.
     *
     * @return array
     */
    public static function getCredentials(Request $request = null, $strict = true)
    {
        if (!($request instanceof Request)) {
            $request = Request::createFromGlobals();
        }

        if ($authorizationHeader = $request->headers->get('Authorization')) {
            $credentials = self::parseAuthorizationHeader($authorizationHeader);
        }
        elseif ($strict) {
            throw new \Exception("Authorization header is required.");
        }
        else {
            $credentials = self::parseRequest($request);
        }

        $data = $request->request->all();
        $urlLength = (strpos($request->getUri(), '?') !== false) ? strpos($request->getUri(), '?') : strlen($request->getUri());
        $data = array_merge($data, array(
            'api-key' => $credentials['api-key'],
            'api-method' => $request->getMethod(),
            'api-url' => substr($request->getUri(), 0, $urlLength),
        ));
        ksort($data);

        $credentials['api-data'] = json_encode($data);

        return $credentials;
    }

    /**
     * Parse an authorization header.
     *
     * @param string $header An authorization header.
     *
     * @return array
     */
    public static function parseAuthorizationHeader($header = null)
    {
        // If no header is provided, attempt to get one
        $header = empty($header) ? self::getAuthorizationHeader() : $header;

        if (!$header) {
            return;
        }

        // Parse the credentials
        $credentials = array();
        foreach (explode(',', $header) as $credential) {
            list($key, $value) = explode('=', trim($credential));
            $credentials['api-' . $key] = empty($value) ? '' : $value;
        }

        // Check for valid credentials
        foreach (array('user', 'key', 'hash') as $credential) {
            if (empty($credentials['api-' . $credential])) {
                throw new \UnexpectedValueException(sprintf('Authorization header missing %s value.', $credential));
            }
        }

        return $credentials;
    }

    /**
     * Parse the request for credentials, in case the prefered authorization
     * header method isn't being used.
     *
     * @param Symfony\Component\HttpFoundation\Request $request A Symfony request instance.
     *
     * @return array
     */
    public static function parseRequest(Request $request = null)
    {
        if (!($request instanceof Request)) {
            $request = Request::createFromGlobals();
        }

        // Check for a $_POST value of each credential, defaulting to the $_GET value
        $credentials = array();
        foreach (array('user', 'key', 'hash') as $credential) {
            $credentials['api-' . $credential] = $request->request->get(
                $credential,
                trim(stripslashes($request->query->get($credential)))
            );
        }

        // Check for valid credentials
        foreach (array('user', 'key', 'hash') as $credential) {
            if (empty($credentials['api-' . $credential])) {
                throw new \UnexpectedValueException(sprintf('Request missing API %s value.', $credential));
            }
        }

        return $credentials;
    }

    /**
     * Validate a given API key and secret against a credentials array.  Throws
     * an exception if credentials are invalid.
     *
     * @param string $apiKey The API key to validate.
     * @param string $apiSecret The API secret with which to validate.
     * @param array $credentials A set of credentials.
     */
    public static function validate($apiKey, $apiSecret, $credentials = null)
    {
        $credentials = is_null($credentials) ? self::getCredentials() : $credentials;

        // Validate the provided key
        if (!$apiKey || $apiKey !== $credentials['api-key']) {
            throw new \Exception(
                sprintf('API key "%s" is invalid.', $credentials['api-key'])
            );
        }

        // Validate the provided hash against the hashed API key + secret
        if (hash_hmac('sha256', $credentials['api-data'], $apiSecret) !== $credentials['api-hash']) {
            throw new \Exception(
                sprintf('API hash is invalid for data: %s', $credentials['api-data'])
            );
        }
    }
}
